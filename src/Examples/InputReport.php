<?php

namespace CleverTIC\Report\Examples;

use CleverTIC\Report\JasperReport;

/**
 * Report class to test request input acceptance
 * test this way: http://localhost:8000/report/example.input/generate?a=3&b=4
 * test this way: http://localhost:8000/report/example.input/generate/3/4
 */
class InputReport extends JasperReport
{

  protected $a;
  protected $b;

  /**
   * @see parent::_initJrxmlPath
   */
  protected function _initJrxmlPath()
  {
    return base_path('vendor/clevertic/jasperphp/examples/hello_world.jrxml');
  }

  /**
   * @see parent::_initAttributes
   */
  protected function _initAttributes()
  {
    return ['php_version' => $this->a + $this->b];
  }

  /**
   * @see parent::input
   */
  public function input($input)
  {
    $this->a = array_get($input, 'a');
    $this->b = array_get($input, 'b');
    if (is_null($this->a) || is_null($this->b)) $this->setInvalidInput();
  }

}

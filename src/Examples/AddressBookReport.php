<?php

namespace CleverTIC\Report\Examples;

use CleverTIC\Report\JasperDatasourceReport;


class AddressBookReport extends JasperDatasourceReport
{

  /**
   * @see parent::$datasource_type
   */
  public $datasource_type = 'xml';

  /**
   * XML X-path
   * @var string
   */
  public $xml_xpath = '/addressbook';

  /**
   * @see parent::_initJrxmlPath
   */
  protected function _initJrxmlPath()
  {
    return base_path('vendor/clevertic/jasperphp/examples/addressbook_xml.jrxml');
  }

  /**
   * @see parent::_initAttributes
   */
  protected function _initAttributes()
  {
    return ['php_version' => phpversion()];
  }

  /**
   * @see parent::input
   */
  public function input($input)
  {
    $this->dataset = [
      'addressbook' => [
        'title' => 'PRUEBA',
        'person' => [
          [
            'name' => 'ETHAN',
            'phone' => '+1 (415) 111-1111'
          ],
          [
            'name' => 'CALEB',
            'phone' => '+1 (415) 222-2222'
          ],
          [
            'name' => 'WILLIAM',
            'phone' => '+1 (415) 333-3333'
          ]
        ]
      ]
    ];
  }

}

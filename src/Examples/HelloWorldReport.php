<?php

namespace CleverTIC\Report\Examples;

use CleverTIC\Report\JasperReport;


class HelloWorldReport extends JasperReport
{

  /**
   * @see parent::_initJrxmlPath
   */
  protected function _initJrxmlPath()
  {
    return base_path('vendor/clevertic/jasperphp/examples/hello_world.jrxml');
  }

  /**
   * @see parent::_initAttributes
   */
  protected function _initAttributes()
  {
    return ['php_version' => phpversion()];
  }

}

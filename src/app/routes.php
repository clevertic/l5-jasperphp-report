<?php

Route::get('/report/{type}/generate', ['as' => 'clevertic.report.generate', 'uses' => 'CleverTIC\Report\Controllers\ReportController@generateAndShow']);

if (app()->isLocal()){

  Route::get('/clevertic/report', function(){
    return 'It works!';
  });

  Route::get('/test/report', 'CleverTIC\Report\Controllers\ReportController@test');

  // Route to test example.input with route parameters
  Route::get('/report/{type}/generate/{a}/{b}', ['as' => 'clevertic.report.generate', 'uses' => 'CleverTIC\Report\Controllers\ReportController@generateAndShow']);

}

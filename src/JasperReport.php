<?php

namespace CleverTIC\Report;

use JasperPHP\JasperPHP;


class JasperReport
{

  /**
   * Report type
   * @var string
   */
  public $type;

  /**
   * Bridge to JasperStarter
   * @var JasperBridge
   */
  public $bridge;

  /**
   * Invalid input flag
   * @var boolean
   */
  protected $_invalid_input;

  /**
   * Class constructor
   * @param string $type
   * @param mixed $params
   */
  public function __construct($type = '', $params = null)
  {
    $this->type = $type;
    $this->setInvalidInput(false);
    $this->input($params);
    $this->_initBridge();
  }

  /**
   * Init Jasper Bridge
   */
  protected function _initBridge()
  {
    $this->bridge = new JasperBridge;
    $this->bridge->jrxml_path = $this->_initJrxmlPath();
    $this->bridge->output_path = $this->_initOutputPath();
    $this->bridge->attributes = $this->_initAttributes();
  }

  /**
   * Init Jasper Bridge JRXML path
   */
  protected function _initJrxmlPath()
  {
    return '';
  }

  /**
   * Init Jasper Bridge output path
   */
  protected function _initOutputPath()
  {
    $path = config('report.output_path') .'/'. $this->getOutputFilename();
    $dir = pathinfo($path, PATHINFO_DIRNAME);
    if (!\File::exists($dir)) \File::makeDirectory($dir, 0755, true);
    return $path;
  }

  /**
   * Calculate output filename
   * @return string
   */
  public function getOutputFilename()
  {
    return pathinfo($this->bridge->jrxml_path, PATHINFO_FILENAME);
  }

  /**
   * Init Jasper Bridge JRXML path
   */
  protected function _initAttributes()
  {
    return [];
  }

  /**
   * Set Jasper bridge
   * @param JasperBridge $bridge
   */
  public function setBridge($bridge)
  {
    $this->bridge = $bridge;
  }

  /**
   * Get Jasper bridge
   * @return JasperBridge
   */
  public function getBridge(){
    return $this->bridge;
  }

  /**
   * Report factory
   * @param string $type
   * @param mixed $params
   * @return JasperReport|null
   */
  static public function factory($type, $params)
  {
    $class = config("report.classes.$type");
    if (empty($class)) return null;
    $class = "\\$class";
    $report = new $class($type, $params);
    return $report;
  }

  /**
   * Set input parameters
   * @param [string => mixed] $input
   */
  public function input($input)
  {
    //
  }

  /**
   * Generate report
   * @return string View path
   */
  public function generate()
  {
    setlocale(LC_CTYPE, '');
    setlocale(LC_CTYPE, 'UTF8', 'es_ES.UTF-8');
    $jasper = new JasperPHP;
    $jasper->process(
      $this->bridge->jrxml_path,
      $this->bridge->output_path,
      $this->bridge->formats,
      $this->bridge->attributes,
      $this->bridge->jstarter_args,
      $this->bridge->db_connection,
      $this->bridge->background,
      $this->bridge->redirect_output
    )->execute();
    // return an output view
    return $this->getViewPath();
  }

  /**
   * Get view path
   * @return string
   */
  public function getViewPath()
  {
    return $this->bridge->output_path .".". $this->bridge->formats[0];
  }

  /**
   * Check if input is invalid
   * @return boolean
   */
  public function invalidInput()
  {
    return $this->_invalid_input;
  }

  /**
   * Set as invalid input
   * @param boolean $value
   */
  public function setInvalidInput($value = true)
  {
    $this->_invalid_input = $value;
  }

}

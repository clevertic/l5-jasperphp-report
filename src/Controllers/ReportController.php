<?php

namespace CleverTIC\Report\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JasperPHP\JasperPHP;
use CleverTIC\Report\JasperReport;


class ReportController extends Controller
{

  /**
   * Generate report
   */
  public function generateAndShow(Request $request)
  {
    $report = $this->reportInstance($request);
    $report->generate();
    return $this->show($report);
  }


  /**
   * Get report instance from request
   * @return JasperReport
   */
  protected function reportInstance($request)
  {
    $input = array_merge(\Route::getCurrentRoute()->parameters(), $request->all());
    $report = JasperReport::factory($input['type'], $input);
    if (empty($report)) abort(500);
    if ($report->invalidInput()) abort(404);
    return $report;
  }

  /**
   * Show report as application/pdf
   * @param JasperReport $report
   */
  protected function show($report)
  {
    $view_path = $report->getViewPath();
    return \Response::make(file_get_contents($view_path), 200, [
      'Content-Type' => 'application/pdf',
      'Content-Disposition' => "inline; $view_path",
    ]);
  }





}
